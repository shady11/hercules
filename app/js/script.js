$(window).on('load',function () {
    // Cookie
    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    if(getCookie('active_tab') == 'tasks-tab'){
        $('#tasks-tab').addClass('active');
        $('#tasks-tab').attr('aria-selected', true);
        $('#tasksTabContent').addClass('active show');
    } else if(getCookie('active_tab') == 'users-tab'){
        $('#users-tab').addClass('active');
        $('#users-tab').attr('aria-selected', true);
        $('#usersTabContent').addClass('active show');
    } else{
        $('#actions-tab').addClass('active');
        $('#actions-tab').attr('aria-selected', true);
        $('#actionsTabContent').addClass('active show');
    }

    if(getCookie('nav_toggle') == 1){
        $('body').addClass('nav-toggled');
    } else {
        $('body').removeClass('nav-toggled');
    }

    if(getCookie('timeline_toggle') == 1){
        $('body').addClass('timeline-toggled');
    } else {
        $('body').removeClass('timeline-toggled');
    }

    // Nav Toggle
    var navToggler = $('.nav-toggler'),
        sidebar = $('.sidebar');

    navToggler.click(function (e) {
        e.preventDefault();
        $('body').toggleClass('nav-toggled');
        if($('body').hasClass('nav-toggled'))
            document.cookie = "nav_toggle=1";
        else
            document.cookie = "nav_toggle=0";
    });


    // Timeline
    var timeline = $('.timeline'),
        timelineHeader = $('.timeline-header'),
        timelineContent = $('.timeline-content'),
        timelineTab = $('#timelineTab .nav-link'),
        timelineToggler = $('#timelineToggler');

    timelineContent.css('height', timeline.outerHeight()-timelineHeader.outerHeight());

    timelineTab.click(function (e) {
        e.preventDefault();
        document.cookie = "active_tab="+$(this).attr('id');
    });

    timelineToggler.click(function (e) {
        e.preventDefault();
        $('body').toggleClass('timeline-toggled');
        if($('body').hasClass('timeline-toggled'))
            document.cookie = "timeline_toggle=1";
        else
            document.cookie = "timeline_toggle=0";
    });

    // Input Validation
    $('input.form-control').on('input', function(){
        if ($(this).val()){
            $(this).addClass("has-value");
        } else {
            $(this).removeClass("has-value");
        }
    });
    $('input.form-control').each(function(){
        if ($(this).val()){
            $(this).addClass("has-value");
        } else {
            $(this).removeClass("has-value");
        }
    });

    // Bootstrap Select
    $('.selectpicker').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        $(this).parent().addClass('has-value');
    });

    // Autocomplete
    $('.form-control-autocomplete').autocomplete({
        maximumItems: 3,
        onSelectItem: function(item, element){
            $(element).addClass("has-value");
        },
        source: {
            "Петров Александр Иванович": 1,
            "Smith Sarah": 2,
            "Сергеев Петр Александрович": 3
        },
        treshold: 0
    });
    $('.form-control-autocomplete-avatar').autocomplete({
        maximumItems: 3,
        onSelectItem: function(item, element){
            $(element).addClass("has-value");
        },
        source: {
            "<div class='d-flex align-items-center'><div class='avatar avatar-sm'><img class='avatar-img rounded-circle' src='assets/img/avatar-6.jpg' alt=''></div><p>Петров Александр Иванович</p></div>": 1,
            "<div class='d-flex align-items-center'><div class='avatar avatar-sm'><img class='avatar-img rounded-circle' src='assets/img/avatar-8.jpg' alt=''></div><p>Smith Sarah</p></div>": 2,
            "<div class='d-flex align-items-center'><div class='avatar avatar-sm'><img class='avatar-img rounded-circle' src='assets/img/avatar-10.jpg' alt=''></div><p>Сергеев Петр Александрович</p></div>": 3
        },
        treshold: 0
    });
});